import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './features/login/login.component';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot([
      { path: 'home', loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule )},
      { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
      { path: 'admin', loadChildren: () => import('./features/admin/admin.module').then(m => m.AdminModule) },
      { path: '', pathMatch: 'full', redirectTo: 'home' },
      { path: 'users', loadChildren: () => import('./features/users/users.module').then(m => m.UsersModule) },
      { path: 'meteo', loadChildren: () => import('./features/meteo-example/components-demo.module').then(m => m.ComponentsDemoModule) },
      { path: 'children', loadChildren: () => import('./features/components-children/components-children.module').then(m => m.ComponentsChildrenModule) },
      { path: 'directives', loadChildren: () => import('./features/directives/directives.module').then(m => m.DirectivesModule) },
      { path: '**', redirectTo: 'home'},
    ]),
  ],
  exports: [
    RouterModule,
  ]
})
export class AppRoutingModule { }
