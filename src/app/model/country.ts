export interface Country {
  id: number;
  name: string;
  cities: City[]
}

export interface City {
  id: number;
  label: string;
  coords: {
    lat: number,
    lng: number
  }
}
