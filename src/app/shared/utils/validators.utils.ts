
// ==================================
// SYNC EMAIL VALIDATOR
// ==================================
import { FormControl, ValidationErrors } from '@angular/forms';

export const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export function emailValidator(c: FormControl): ValidationErrors {
  if (c.value && c.value.indexOf('@') === -1 ) {
    return { missingAt: true };
  }
  if (c.value && !c.value.match(EMAIL_REGEX)) {
    return { invalidEmail: true };
  }
  // no errors
  return null;
}
