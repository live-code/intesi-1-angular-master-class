import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './components/card.component';
import { CardModule } from './components/card.module';
import { ShowIfLengthPipe } from './pipes/show-if-length.pipe';
import { SharedPipesModule } from './pipes/shared-pipes.module';
import { AccordionComponent } from './components/accordion.component';
import { AccordionGroupComponent } from './components/accordion-group.component';
import { GooglemapComponent } from './components/googlemap.component';
import { ChartJsComponent } from './components/chart-js.component';
import { UrlDirective } from './directives/url.directive';
import { RoleDirective } from './directives/role.directive';
import { RolePoweredDirective } from './directives/role-powered.directive';



@NgModule({
  declarations: [
    AccordionComponent,
    AccordionGroupComponent,
    GooglemapComponent,
    ChartJsComponent,
    UrlDirective,
    RoleDirective,
    RolePoweredDirective
  ],
  imports: [
    CommonModule,
    CardModule,
    SharedPipesModule
  ],
  exports: [
    CardModule,
    SharedPipesModule,
    AccordionComponent,
    AccordionGroupComponent,
    GooglemapComponent,
    ChartJsComponent,
    UrlDirective,
    RoleDirective,
    RolePoweredDirective
    // ButtonModule
  ]
})
export class SharedModule { }
