import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { MyService } from '../../features/directives/directives.component';

@Directive({
  selector: '[intRolePowered]'
})
export class RolePoweredDirective {
  @Input() intRolePowered: string;

  constructor(
    private template: TemplateRef<any>,
    private view: ViewContainerRef,
    private myService: MyService
  ) {

    this.myService.role$
      .subscribe(role => {
        if (role === this.intRolePowered) {
          view.createEmbeddedView(template)
        } else {
          view.clear();
        }
      });
  }

}

