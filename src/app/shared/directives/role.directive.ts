import { Directive, ElementRef, HostBinding, Input, OnDestroy, Renderer2 } from '@angular/core';
import { MyService } from '../../features/directives/directives.component';
import { Subscription } from 'rxjs';

@Directive({
  selector: '[intRole]'
})
export class RoleDirective implements OnDestroy {
  @Input() intRole: string;
  sub: Subscription;

/*  @HostBinding('style.display') get display(): string {
    return this.intRole === this.myService.role
          ? 'block' : 'none';
  }*/
  constructor(
    public myService: MyService,
    private el: ElementRef,
    private renderer2: Renderer2
  ) {
    this.sub = this.myService.role$
      .subscribe(val => {
        console.log(val)
        if (val) {
          renderer2.setStyle(el.nativeElement, 'display', 'block')
        } else {
          renderer2.setStyle(el.nativeElement, 'display', 'none')
        }
      });
  }

  ngOnDestroy(): void {
    console.log('destroy')
    this.sub.unsubscribe();
  }

}
