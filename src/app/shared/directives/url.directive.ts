import { Directive, ElementRef, HostBinding, HostListener, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[intUrl]'
})
export class UrlDirective {
  @Input() intUrl;
  @Input() set bg(val: string) {
    this.rendered.setStyle(this.el.nativeElement, 'backgroundColor', val);
  }
  @HostBinding('style.cursor') cursor = 'pointer';
  @HostBinding('style.fontSize') fontSize = '30px';

  @HostListener('click')
  clickMe(): void {
    window.open(this.intUrl);
  }

  constructor(private el: ElementRef, private rendered: Renderer2) {
  }
}
