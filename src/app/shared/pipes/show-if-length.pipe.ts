import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'showIfLength',
})
export class ShowIfLengthPipe implements PipeTransform {

  transform(text: string, minChars: number): boolean {
    return text.length > minChars;
  }

}
