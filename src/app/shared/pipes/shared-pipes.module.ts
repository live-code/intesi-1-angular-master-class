import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShowIfLengthPipe } from './show-if-length.pipe';



@NgModule({
  declarations: [ShowIfLengthPipe],
  exports: [ShowIfLengthPipe],
  imports: [
    CommonModule
  ]
})
export class SharedPipesModule { }
