import {
  AfterContentInit,
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ContentChildren, DoCheck,
  OnInit,
  Query,
  QueryList,
  ViewChildren
} from '@angular/core';
import { AccordionGroupComponent } from './accordion-group.component';

@Component({
  selector: 'int-accordion',
  template: `
    
    <div style="border-radius: 20px; border: 1px solid blue; padding: 20px">
      <ng-content></ng-content>
    </div>
    
  `,
  styles: [
  ]
})
export class AccordionComponent  implements DoCheck {
  @ContentChildren(AccordionGroupComponent) groups: QueryList<AccordionGroupComponent>;
  previousGroup: QueryList<AccordionGroupComponent>;

  ngDoCheck(): void {
    if (this.previousGroup !== this.groups) {
      this.init();
    }
    this.previousGroup = this.groups;
  }

  init(): void {
    console.log(this.groups.toArray());

    this.groups.forEach(group => {
      group.opened = false;
      group.toggle.subscribe(() => {
        this.closeAll();
        group.opened = true;
      });
    });
    this.groups.toArray()[0].opened = true;
  }

  closeAll(): void {
    this.groups.forEach(group => {
      group.opened = false;
    });
  }

}



/*


@Component({
  selector: 'fb-accordion-group',
  template: `
    <div class="group">
      <div class="title"
           (click)="toggle.emit()"
           >{{title}}</div>
      <div class="body" *ngIf="opened">
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styles: [`
    .title {
      background-color: #ccc;
      padding: 0.8rem;
      cursor: pointer;
      border-bottom: 1px solid #666;
    }
    .body {
      border: 1px solid #ccc;
      padding: 0.8rem;
    }
  `]
})
export class AccordionGroupComponent {
  @Input() title: string;
  @Input() opened = false;
  @Output() toggle: EventEmitter<void> = new EventEmitter<void>();
}*/
