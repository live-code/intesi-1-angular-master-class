import { ChartConfiguration } from 'chart.js';

export const config: ChartConfiguration = {
  type: 'radar',
  data: {
    labels: ['Mon', 'Tue', 'Sleeping', 'Designing', 'Coding', 'Cycling', 'Running'],
    datasets: [{
      label: 'My First Dataset',
      data: [],
      backgroundColor: 'rgba(255, 99, 132, 0.2)',
      borderColor: 'rgb(255, 99, 132)',
      pointBackgroundColor: 'rgb(255, 99, 132)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgb(255, 99, 132)'
    }]
  },
  options: {
    elements: {
      line: {
        borderWidth: 3
      }
    }
  },
};
