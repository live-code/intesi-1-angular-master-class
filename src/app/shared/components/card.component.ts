import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'int-card',
  template: `
    <div class="card">
      <div class="card-header" 
           (click)="open = !open">
        {{title}}
        <i 
          class="pull-right"
          [ngClass]="icon"
          (click)="iconClick.emit()"
        ></i>
      </div>
      <!--<div class="card-body" *ngIf="title | showIfLength: 4">-->
      <div class="card-body" *ngIf="open">
        <ng-content></ng-content>
      </div>
    </div>
  `,
})
export class CardComponent  {
  @Input() title: string;
  @Input() url: string;
  @Input() icon: string;
  @Output() iconClick = new EventEmitter();

  @Input() open = true;

}
