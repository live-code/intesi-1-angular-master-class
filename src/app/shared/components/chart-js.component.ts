import { AfterViewInit, Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import {
  CategoryScale,
  Chart,
  ChartConfiguration,
  LinearScale,
  LineController,
  LineElement,
  PointElement,
  RadarController,
  RadialLinearScale
} from 'chart.js';
import { config } from './chartjs.helper';
Chart.register(RadarController, RadialLinearScale, LineController, LineElement, CategoryScale, PointElement, LinearScale);

@Component({
  selector: 'int-chart-js',
  template: `
    <div>
      <canvas width="400" height="300" #host></canvas>
    </div>
  `,
  styles: [
  ]
})
export class ChartJsComponent implements OnChanges {
  @ViewChild('host', { static: true }) host: ElementRef<HTMLCanvasElement>;
  @Input() lineColor: string;
  @Input() temps: number[];
  myChart: Chart;

  init(): void {
    this.myChart = new Chart(this.host.nativeElement.getContext('2d'), config);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.myChart) {
      this.init();
    }
    if (changes.temps) {
      this.myChart.data.datasets[0].data = changes.temps.currentValue
    }
    if (changes.lineColor) {
      this.myChart.data.datasets[0].borderColor = this.lineColor;
    }
    this.myChart.update();
  }

}


