import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'int-accordion-group',
  template: `
    <div class="card">
      <div class="card-header" (click)="toggle.emit()">
        {{title}}
      </div>
      <div class="card-body" *ngIf="opened">
        <ng-content></ng-content>
      </div>
    </div>
  `,
})
export class AccordionGroupComponent  {
  @Input() title: string;
  @Output() toggle = new EventEmitter();
  @Input() opened = true;

}
