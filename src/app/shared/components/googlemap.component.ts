import { AfterViewInit, Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';


@Component({
  selector: 'int-googlemap',
  template: `
   <div #host style="width: 100%; height: 300px"></div>
  `,
  styles: [
  ]
})
export class GooglemapComponent implements OnChanges {
  @ViewChild('host', { static: true }) host: ElementRef<HTMLDivElement>;
  @Input() coords: {  lat: number, lng: number };
  @Input() zoom = 4;

  map: google.maps.Map;

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.map) {
      this.map = new google.maps.Map(this.host.nativeElement);
    }

    if (changes.coords) {
      const myLatLng = new google.maps.LatLng(this.coords.lat, this.coords.lng);
      this.map.setCenter(myLatLng)
    }

    if (changes.zoom) {
      this.map.setZoom(this.zoom);
      this.map.setZoom(changes.zoom.currentValue);
    }
  }

}
