import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'int-tabbar',
  template: `
    <ul class="nav nav-tabs">
      <li class="nav-item" *ngFor="let item of items">
        <a class="nav-link" aria-current="page"
           [ngClass]="{'active': item.id === this.active?.id}"
           (click)="tabClick.emit(item)">
          {{item[labelField]}}
        </a>
      </li>
    </ul>
  `,
  styles: [
  ]
})
export class TabbarComponent<T> {
  @Input() items: T[];
  @Input() active: T;
  @Input() labelField = 'name';
  @Output() tabClick = new EventEmitter<T>();
}
