import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card.component';
import { TabbarComponent } from './tabbar.component';
import { ShowIfLengthPipe } from '../pipes/show-if-length.pipe';
import { SharedPipesModule } from '../pipes/shared-pipes.module';
import { AccordionComponent } from './accordion.component';



@NgModule({
  declarations: [CardComponent, TabbarComponent],
  exports: [CardComponent, TabbarComponent],
  imports: [
    CommonModule,
    SharedPipesModule
  ]
})
export class CardModule { }
