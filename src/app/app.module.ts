import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavbarComponent } from './core/navbar.component';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { CardComponent } from './shared/components/card.component';
import { HttpClientModule } from '@angular/common/http';
import { ThemeService } from './core/theme.service';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    // ThemeService,
    { provide: ThemeService, useClass: ThemeService },
    // { provide: LogService, useClass: GoogleAnalyticsService},
    /*{
      provide: LogService, useFactory: () => {
        return environment.production ? new GoogleAnalyticsService() : new FakeLog()
      }
    },*/
  ],
  exports: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
