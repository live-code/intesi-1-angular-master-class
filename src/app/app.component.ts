import { Component } from '@angular/core';

@Component({
  selector: 'int-root',
  template: `
    
    <int-navbar></int-navbar>
    <hr>
    <router-outlet></router-outlet>
    
  `,
  styles: []
})
export class AppComponent {
  title = 'intesi';
}
