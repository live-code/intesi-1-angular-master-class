import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'int-navbar',
  template: `
    <button routerLink="home">home</button>
    <button routerLink="login">login</button>
    <button routerLink="users">users</button>
    <button [routerLink]="'admin'">admin</button>
    <button [routerLink]="'meteo'">meteo</button>
    <button [routerLink]="'directives'">directives</button>
    <button [routerLink]="'children'">children</button>
    
  `,
  styles: [
  ]
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
