import { Injectable } from '@angular/core';

@Injectable()
export class ThemeService {
  value = 'dark';
  constructor() {
    setTimeout(() => {
      this.value = 'light';
    }, 1000)
  }
}
