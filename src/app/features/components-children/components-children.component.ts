import { AfterViewInit, Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { CardComponent } from '../../shared/components/card.component';

@Component({
  selector: 'int-components-children',
  template: `
    
    <div bg="red" #something>456</div>
    <div #something>123</div>
    <int-card title="one">bla bla 1</int-card>
    <int-card title="two">bla bla 2</int-card>
  
    <button (click)="closeAll()">close all</button>
    
    <int-accordion>
      <int-accordion-group title="one" >
        bla bla one
      </int-accordion-group>
      <int-accordion-group title="two">
        bla bla one
      </int-accordion-group>
      <int-accordion-group
        *ngFor="let item of list"
        [title]="item.title"
      >
        {{item.data}}
      </int-accordion-group>
    </int-accordion>

  `,
  styles: [
  ]
})
export class ComponentsChildrenComponent implements AfterViewInit {
  @ViewChildren('something') divs: QueryList<ElementRef<HTMLDivElement>>;
  @ViewChildren(CardComponent) cards: QueryList<CardComponent>;
  open1: boolean;
  open2: boolean;

  list = [{title: '3', data: 'bla bla'}, {title: '4', data: 'bla bla'}]

  ngAfterViewInit(): void {
    this.divs.toArray().forEach((item: ElementRef<HTMLDivElement>) => {
      item.nativeElement.innerText = 'pippo';
      item.nativeElement.style.color = 'red';
    });

    this.open1 = true;
    this.open2 = false;
    setTimeout(() => {
      this.cards.toArray().forEach(card => {
        card.open = false;
      });
      this.cards.toArray()[0].open = true;
    });
  }

  closeAll(): void {
    this.cards.toArray().forEach(card => card.open = false)
  }
}
