import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ComponentsChildrenComponent } from './components-children.component';

const routes: Routes = [{ path: '', component: ComponentsChildrenComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComponentsChildrenRoutingModule { }
