import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsChildrenRoutingModule } from './components-children-routing.module';
import { ComponentsChildrenComponent } from './components-children.component';
import { CardModule } from '../../shared/components/card.module';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    ComponentsChildrenComponent
  ],
  imports: [
    CommonModule,
    CardModule,
    SharedModule,
    ComponentsChildrenRoutingModule
  ]
})
export class ComponentsChildrenModule { }
