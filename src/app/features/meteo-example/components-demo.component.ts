import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'int-components-demo',
  template: `
    <int-weather [city]="city" unit="metric"></int-weather>
    <button (click)="city = 'Trieste'">Trieste</button>
    <button (click)="city = 'Rome'">Rome</button>
   <!-- <gmap [coords]="..." [zoom]="12"></gmap>-->
  `,
  styles: [
  ]
})
export class ComponentsDemoComponent implements OnInit {
  city: string;

  constructor() {

  }

  ngOnInit(): void {
  }

}
