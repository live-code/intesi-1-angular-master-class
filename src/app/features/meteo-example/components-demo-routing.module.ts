import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ComponentsDemoComponent } from './components-demo.component';

const routes: Routes = [{ path: '', component: ComponentsDemoComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComponentsDemoRoutingModule { }
