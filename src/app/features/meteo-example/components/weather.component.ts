import { AfterViewInit, Component, ElementRef, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { fromEvent } from 'rxjs';
import { FormControl } from '@angular/forms';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'int-weather',
  template: `
    <h1>Weather of {{city}}</h1>
    <input type="text" [formControl]="input2" #inputRef [readOnly]="pending">
    {{meteo | json}}
  `,
})
export class WeatherComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {
  @ViewChild('inputRef') input: ElementRef<HTMLInputElement>;
  input2 = new FormControl();
  @Input() city: string;
  @Input() unit = 'metric';
  meteo: any;
  pending = false;

  constructor(private http: HttpClient) {
    console.log('ctr', this.city, this.input);
    this.input2.valueChanges.subscribe(console.log)
  }

  ngOnInit(): void {
    console.log('ngOninit', this.city, this.input);
  }

  ngAfterViewInit(): void {
    console.log('ngAfterViewInit', this.city, this.input);
    this.input.nativeElement.focus();
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes)
    if (changes.city) {
      console.log('do something with city', this.city, this.unit)
    }
    if (changes.unit) {
      console.log('do something with unit', this.city)
    }

    if (this.city) {
      this.pending = true;
      console.log('--->', this.city, this.unit)
      this.http.get(`http://api.openweathermap.org/data/2.5/weather?q=${this.city}&units=${this.unit}&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
        .pipe(
          delay(2000)
        )
        .subscribe(res => {
          this.pending = false;
          this.meteo = res;
        });
    }

  }

  ngOnDestroy(): void {
    console.log('ngOnDestroy');
  }


}
