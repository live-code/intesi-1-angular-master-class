import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsDemoRoutingModule } from './components-demo-routing.module';
import { ComponentsDemoComponent } from './components-demo.component';
import { WeatherComponent } from './components/weather.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ComponentsDemoComponent,
    WeatherComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ComponentsDemoRoutingModule
  ]
})
export class ComponentsDemoModule { }
