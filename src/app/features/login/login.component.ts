import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'int-login',
  template: `
    <h1>Login {{activeUrl}}</h1>
    <router-outlet></router-outlet>
    <br>
    <button routerLink="signin" >SignIn</button>
    <button routerLink="registration">Registration</button>
    <button routerLink="lostpass">LostPass</button>
  `,
  styles: [
  ]
})
export class LoginComponent implements OnInit {
  activeUrl: string;

  constructor(private activate: ActivatedRoute, private router: Router) {
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd)
      )
      .subscribe((ev: NavigationEnd) => this.activeUrl = ev.url);
  }

  ngOnInit(): void {
  }

}
