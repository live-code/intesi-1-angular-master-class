import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { SignInComponent } from './components/sign-in.component';
import { RegistrationComponent } from './components/registration.component';
import { LostPassComponent } from './components/lost-pass.component';


@NgModule({
  declarations: [
    LoginComponent,
    SignInComponent,
    RegistrationComponent,
    LostPassComponent
  ],
  imports: [
    CommonModule,
    LoginRoutingModule
  ]
})
export class LoginModule { }
