import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'int-sign-in',
  template: `
    <input type="text">
    <input type="text">
    <button>SIGNIN</button>
    <hr>
  `,
  styles: [
  ]
})
export class SignInComponent implements OnInit, OnDestroy {

  constructor() {
    console.log('constructor')
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    console.log('destroy')
  }

}
