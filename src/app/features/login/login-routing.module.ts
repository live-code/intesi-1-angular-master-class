import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login.component';
import { RegistrationComponent } from './components/registration.component';
import { SignInComponent } from './components/sign-in.component';
import { LostPassComponent } from './components/lost-pass.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
    children: [
      { path: 'signin', component: SignInComponent },
      { path: 'lostpass', component: LostPassComponent },
      { path: 'registration', component: RegistrationComponent },
      { path: '', redirectTo: 'signin' },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
