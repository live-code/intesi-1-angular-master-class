import { Component, Injectable, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'int-directives',
  template: `
    Visit this website:
    <span intUrl="http://www.google.com" [bg]="background">Google</span>
    <span intUrl="http://www.microsoft.com" >Microsoft</span>
    <button (click)="background = 'pink'">Change COlor</button>
    <button (click)="background = 'cyan'">Change COlor</button>
    
    <hr>
    
    <div intRole="editor">Show me if i am admin</div>

    <div *intRolePowered="'editor'">Powered show me if i am admin</div>

    <div *ngIf="myService.role === 'editor'; else loading ">I'm editor </div>
    <ng-template #loading>
      Loading...
    </ng-template>
    
    Role is: {{myService.role$ | async}}
    <button (click)="getRole()">Get Role</button>
  `,
  styles: [
  ]
})
export class DirectivesComponent {
  background = 'purple';
  role: string;

  constructor(public myService: MyService) {
    myService.role$.subscribe(val => this.role = val)
  }

  getRole(): void {
    console.log(this.myService.role$.getValue());
  }
}

@Injectable({ providedIn: 'root'})
export class MyService {
  ifLogged = false;
  role = 'admin';
  role$ = new BehaviorSubject(null);

  constructor() {
    setTimeout(() => {
      this.ifLogged = true;
      this.role = 'editor';
      this.role$.next('admin')
    }, 3000);
    setTimeout(() => {
      this.role$.next(null)
    }, 4000);

    setTimeout(() => {
      this.role$.next('editor')
    }, 5000);

    setTimeout(() => {
      this.role$.next('admin')
    }, 6000);
  }
}
