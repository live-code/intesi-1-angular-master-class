import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { CommonModule } from '@angular/common';
import { NewsComponent } from './components/news.component';
import { CarouselComponent } from './components/carousel.component';
import { RouterModule } from '@angular/router';
import { HomeRoutingModule } from './home-routing.module';
import { CardComponent } from '../../shared/components/card.component';
import { SharedModule } from '../../shared/shared.module';
import { CardModule } from '../../shared/components/card.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    HomeComponent,
    NewsComponent,
    CarouselComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    HomeRoutingModule
  ]
})
export class HomeModule {

}
