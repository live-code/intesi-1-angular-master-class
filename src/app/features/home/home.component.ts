import { Component, OnInit } from '@angular/core';
import { City, Country } from '../../model/country';

@Component({
  selector: 'int-home',
  template: `
    <h1>Home</h1>
    <int-carousel></int-carousel>
    <int-news></int-news>
   
    <int-card 
      title="Forms" 
      url="http://www.microsoft.com"
      icon = 'fa fa-link'
      (iconClick)="openUrl('http://www.google.com')"
    >
      <input type="text" ngModel>
    </int-card>
    
    <int-card 
      title="ABC"
      url="http://www.google.com"
      icon = 'fa fa-bluetooth'
      (iconClick)="log('http://www.google.com')"
    >
      <input type="text" ngModel>
    </int-card>
    
    
    
    <int-tabbar 
      [items]="countries" 
      [active]="activeCountry"
      (tabClick)="selectCountry($event)"
    ></int-tabbar>
    
    <int-tabbar
      *ngIf="activeCountry"
      [items]="activeCountry.cities"
      [active]="activeCity"
      (tabClick)="selectCity($event)"
      labelField="label"
    ></int-tabbar>
    
    <pre>{{
      activeCity | json
      }}
    </pre>
    <int-googlemap
      [zoom]="zoom"
      [coords]="{ lat: activeCity?.coords.lat, lng: activeCity?.coords.lng}"
    ></int-googlemap>
    <button (click)="zoom = zoom - 1">-</button>
    <button (click)="zoom = zoom + 1">+</button>
    
    <hr>
    

    <int-chart-js
      [lineColor]="lineColor"
      [temps]="temps"></int-chart-js>

    <button (click)="temps = [11, 12, 32, 4, 4, 423, 32]">Temp 1</button>
    <button (click)="temps = [33, 24, 32, 44, 14, 1, 32]">Temp 2</button>
    <button (click)="lineColor = 'green'">green</button>
    <button (click)="lineColor = 'cyan'">cyan</button>
  `,
  styles: [
  ]
})
export class HomeComponent implements OnInit {
  temps = [111, 132, 342, 45, 45, 23, 32];
  lineColor = 'pink';
  zoom = 10;
  coords = { lat: 43, lng: 13};

  countries: Country[] = [
    {
      id: 1,
      name: 'Italy',
      cities: [
        { id: 1001, label: 'Milan', coords: { lat: 41, lng: 12 } },
        { id: 1002, label: 'Rome', coords: { lat: 42, lng: 13 } } ,
        { id: 1003, label: 'Milan', coords: { lat: 43, lng: 14 } }
      ]

    },
    {
      id: 2,
      name: 'Germany',
      cities: [
        { id: 10011, label: 'Berlino', coords: { lat: 55, lng: 33 } },
      ]
    },
    {
      id: 3,
      name: 'UK',
      cities: [
        { id: 10042, label: 'London', coords: { lat: 66, lng: 22 } },
        { id: 10072, label: 'Leeds', coords: { lat: 66, lng: 22 } },
      ]
    },
  ];
  activeCountry: Country;
  activeCity: City;

  ngOnInit(): void {
    this.selectCountry(this.countries[0])

    setTimeout(() => {
      this.coords = { lat: 44, lng: 13}
    }, 3000)
  }


  openUrl(url: string): void {
    window.open(url);
  }
  log(url: string): void {
    console.log('something', url)
  }

  selectCountry(country: Country): void {
    this.activeCountry = country;
    this.activeCity = country.cities[0];
  }

  selectCity(city: City): void {
    this.activeCity = city;
  }
}
