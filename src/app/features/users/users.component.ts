import { AfterViewInit, Component, ElementRef, Injectable, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  AbstractControl,
  AsyncValidatorFn,
  FormBuilder,
  FormControl,
  FormGroup,
  NgForm,
  ValidationErrors,
  Validators
} from '@angular/forms';
import { combineLatest, fromEvent, Observable, timer } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, mergeAll, mergeMap, switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'int-users',
  template: `
    
    <form [formGroup]="form">
      <pre>{{form.get('username').errors | json}}</pre>
      <pre>{{form.get('username').pending | json}}</pre>
      <input class="form-control" type="text" formControlName="username">
      
      <pre>{{form.get('email').errors | json}}</pre>
      <input class="form-control" type="text" formControlName="email">
      
      
      <ng-container formGroupName="address">
        <h1>
          <i class="fa fa-check" *ngIf="form.get('address').valid"></i>
          Location
        </h1>
        <input class="form-control" type="text" formControlName="city"
               [ngClass]="{'is-invalid': form.get('address').get('city').invalid}">
        <input class="form-control" type="text" formControlName="street">
      </ng-container>
      
      
      <button [disabled]="form.invalid || form.pending">save</button>
    </form>
    
    <ul>
      <li *ngFor="let user of users">
        {{user.username}}
      </li>
    </ul>
  `,
  styles: [
  ]
})
export class UsersComponent implements OnInit {
  form: FormGroup;
  users: any[];

  constructor(private http: HttpClient, private fb: FormBuilder, private userValidators: UserValidator) {
    this.form = fb.group({
      username: [
        '',
        Validators.required,
        userValidators.uniqueUsername()
      ],
      email: [
        '',
        Validators.compose([Validators.required, Validators.minLength(3), emailValidator]),
      ],

      address: fb.group({
        city: ['', Validators.required],
        street: ['', Validators.required],
      })
    });

/*
    this.form.valueChanges
      .pipe(
        debounceTime(1000),
        filter(() => this.form.valid),
        mergeMap((obj) => this.http.post<any>('https://jsonplaceholder.typicode.com/users', obj)),
      )
      .subscribe(res => {
          // this.users.push(res)
      });*/
  }

  ngOnInit(): void {

    this.http.get<any[]>('https://jsonplaceholder.typicode.com/users/1')
      .subscribe(res => {
        this.form.patchValue(res, { emitEvent: false })
      });
  }

}


// ASYNC USERNAME VALIDATOR
@Injectable({ providedIn: 'root'})
export class UserValidator {
  constructor(private http: HttpClient) {
  }

  uniqueUsername(): AsyncValidatorFn {
    return (c: AbstractControl): Observable<ValidationErrors | null> => {
      return timer(1000)
        .pipe(
          switchMap(() => this.http.get<any[]>(`https://jsonplaceholder.typicode.com/users?username=${c.value}`)),
          map(res => res.length > 0 ?  ({ alreadyExists: true }) : null)
        );
    };
  }
}

// SYNC EMAIL VALIDATOR
export const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export function emailValidator(c: FormControl): ValidationErrors {
  if (c.value && c.value.indexOf('@') === -1 ) {
    return { missingAt: true };
  }
  if (c.value && !c.value.match(EMAIL_REGEX)) {
    return { invalidEmail: true };
  }
  // no errors
  return null;
}
