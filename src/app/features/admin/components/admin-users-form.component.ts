import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { AbstractControl, AsyncValidatorFn, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { UserValidator } from '../services/user.validators';
import { User } from '../../../model/user';
import { emailValidator } from '../../../shared/utils/validators.utils';

@Component({
  selector: 'int-admin-users-form',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    {{theme}}
    <form 
      [formGroup]="form" 
      (submit)="save.emit(form.value)"
    >
      <input type="text" *ngFor="let item of json" [formControlName]="item.key">
      
      <input class="form-control" type="text" formControlName="username" placeholder="username">
      <input class="form-control" type="text" formControlName="email"  placeholder="Email">

      <ng-container formGroupName="address">
        <h1>
          <i class="fa fa-check" 
             *ngIf="form.get('address').valid"></i>
          Location
        </h1>
        <input
         placeholder="city"
          class="form-control" type="text" formControlName="city"
         [ngClass]="{'is-invalid': form.get('address').get('city').invalid}">
        <input
          placeholder="street"
          class="form-control" type="text" formControlName="street">
      </ng-container>
      
      <button type="submit" [disabled]="form.invalid || form.pending">
        {{user?.id ? 'EDIT' : 'ADD'}}
      </button>
      
      <button type="button" (click)="clearHandler()">Clear</button>
    </form>
    
  `,
})
export class AdminUsersFormComponent implements OnChanges {
  @Output() save = new EventEmitter();
  @Output() clear = new EventEmitter();
  @Input() user: User;
  @Input() theme: string;

  json = [
    {
      key: 'phone',
      validators: 'required'
    },
    {
      key: 'website',
      validators: null
    },
    {
      type: 'group',
      key: 'carInfo',
      items: [
        {
          key: 'phone',
          validators: 'required'
        },
        {
          key: 'website',
          validators: null
        },
      ]
    }
  ];

  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private userValidators: UserValidator,
  ) {
    this.form = fb.group({
      username: [
        '',
        Validators.required,
        userValidators.uniqueUsername()
      ],
      email: [
        '',
        Validators.compose([Validators.required, Validators.minLength(3), emailValidator]),
      ],

      address: fb.group({
        city: ['', Validators.required],
        street: ['', Validators.required],
      })
    });

    this.json.forEach(item => {
      console.log(item);

      this.form.addControl(
        item.key,
        new FormControl(['bla bla', Validators.compose([
          item.validators === 'required' ? Validators.required : null,
        ])])
      );
    });

  }

  ngOnChanges(): void {
    console.log(this.user)
    if (this.user) {
      this.form.patchValue(this.user, { emitEvent: false })
    } else {
      this.form.reset()
    }
  }

  clearHandler(): void {
    this.form.reset();
    this.clear.emit();
  }
}
