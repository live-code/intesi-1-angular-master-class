import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from '../../../model/user';

@Component({
  selector: 'int-admin-users-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <ul class="list-group">
      <li 
        *ngFor="let user of users"
        class="list-group-item"
        [ngClass]="{active: user.id === activeUser?.id}"
        (click)="itemClick.emit(user)"
      >
        {{user.username}}
        <i 
          class="fa fa-trash" 
          (click)="deleteHandler(user, $event)"
        ></i>
      </li>
    </ul>
    {{render()}}
  `,
  styles: [
  ]
})
export class AdminUsersListComponent {
  @Input() users: User[];
  @Input() activeUser: User;
  @Output() itemClick = new EventEmitter<User>();
  @Output() deleteUser = new EventEmitter<number>();

  render(): void {
    console.log('render', this.users)
  }

  deleteHandler(user: User, event: MouseEvent): void {
    event.stopPropagation();
    this.deleteUser.emit(user.id);
  }
}
