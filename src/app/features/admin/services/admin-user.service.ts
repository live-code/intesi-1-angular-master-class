import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../../model/user';
import { Observable } from 'rxjs';

@Injectable()
export class AdminUserService {
  users: User[];
  activeUser: User;

  constructor(private http: HttpClient) { }

  loadUsers(): void {
    this.http.get<User[]>('https://jsonplaceholder.typicode.com/users')
      .subscribe(res => {
        this.users = res;
      });
  }

  deleteUser(id: number): void {
    this.http.delete(`https://jsonplaceholder.typicode.com/users/${id}`)
      .subscribe(res => {
        this.users = this.users.filter(u => u.id !== id);
        if (id === this.activeUser.id) {
          this.clear();
        }
      });
  }

  save(formData: Pick<User, 'username' | 'email' | 'address'>): void {
    if (this.activeUser) {
      this.edit(formData);
    } else {
      this.add(formData);
    }
  }

  edit(formData: Pick<User, 'username' | 'email' | 'address'>): void {
    this.http.patch<User>(
      `https://jsonplaceholder.typicode.com/users/${this.activeUser.id}`,
      formData
    )
      .subscribe(res => {
        this.users = this.users.map(u => {
          return u.id === this.activeUser.id ? res : u;
        })
      });
  }

  add(formData: Pick<User, 'username' | 'email' | 'address'>): void {
    this.http.post<User>(
      'https://jsonplaceholder.typicode.com/users',
      formData
    )
      .subscribe(res => {
        this.users = [...this.users, res];
      });
  }

  clear(): void {
    this.activeUser = null;
  }
}
