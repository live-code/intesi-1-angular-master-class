import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AbstractControl, AsyncValidatorFn, ValidationErrors } from '@angular/forms';
import { Observable, of, timer } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { User } from '../../../model/user';
import { AdminUserService } from './admin-user.service';

// ASYNC USERNAME VALIDATOR
@Injectable()
export class UserValidator {
  constructor(
    private http: HttpClient,
    private userService: AdminUserService
  ) {
  }

  uniqueUsername(): AsyncValidatorFn {
    return (c: AbstractControl): Observable<ValidationErrors | null> => {
      if (this.userService.activeUser) {
        return of(null);
      }
      return timer(1000)
        .pipe(
          switchMap(() => this.http.get<User[]>(`https://jsonplaceholder.typicode.com/users?username=${c.value}`)),
          map(res => res.length > 0 ?  ({ alreadyExists: true }) : null)
        );
    };
  }
}
