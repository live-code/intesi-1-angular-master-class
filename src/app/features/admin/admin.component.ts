import { AfterViewInit, Component, ElementRef, Injectable, OnInit, ViewChild } from '@angular/core';
import {
  FormControl,
  ValidationErrors,
} from '@angular/forms';
import { ThemeService } from '../../core/theme.service';
import { AdminUserService } from './services/admin-user.service';
import { UserValidator } from './services/user.validators';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'int-admin',
  template: `
    <int-user-management type="admin"></int-user-management>
    <int-user-management type="guest"></int-user-management>
    
  `,
})
export class AdminComponent  {

  constructor(private router: ActivatedRoute) {

  }
}
