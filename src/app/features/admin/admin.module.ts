import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminUsersListComponent } from './components/admin-users-list.component';
import { AdminUsersFormComponent } from './components/admin-users-form.component';
import { AdminUserService } from './services/admin-user.service';
import { UserValidator } from './services/user.validators';
import { UserManagementComponent } from './user-management.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';


@NgModule({
  declarations: [
    AdminComponent,
    AdminUsersListComponent,
    AdminUsersFormComponent,
    UserManagementComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AdminRoutingModule
  ],
  providers: [
    // AdminUserService,
    // UserValidator
  ]
})
export class AdminModule { }
