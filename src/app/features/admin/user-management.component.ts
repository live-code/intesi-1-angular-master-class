import { AfterViewInit, Component, ElementRef, Injectable, Input, OnInit, ViewChild } from '@angular/core';
import {
  FormControl,
  ValidationErrors,
} from '@angular/forms';
import { ThemeService } from '../../core/theme.service';
import { AdminUserService } from './services/admin-user.service';
import { UserValidator } from './services/user.validators';

@Component({
  selector: 'int-user-management',
  template: `
    <int-admin-users-form
      [user]="adminUserService.activeUser"
      [theme]="themeService.value"
      (save)="adminUserService.save($event)"
      (clear)="adminUserService.clear()"
    ></int-admin-users-form>

    <int-admin-users-list
      [users]="adminUserService.users"
      [activeUser]="adminUserService.activeUser"
      (itemClick)="adminUserService.activeUser = $event"
      (deleteUser)="adminUserService.deleteUser($event)"
    ></int-admin-users-list>

  `,
  providers: [
    AdminUserService,
    UserValidator
  ]
})
export class UserManagementComponent implements OnInit {
  @Input() type;

  constructor(
    public themeService: ThemeService,
    public adminUserService: AdminUserService,
  ) {}

  ngOnInit(): void {
    this.adminUserService.loadUsers();
  }
}
